# -*- coding: utf-8 -*-
"""
PROGRAM OUTLINE

1. Grab spectra for stars and save into this direcory.
spectra should be spectral flux density (arbitrary flux/Angstrom) in column 2 vs. 
wavelength (angstrong) in column 1 with 3 lines of header comments. 
Spectra should all be stellar absorption spectra.

2. Main function reads in the data and calls plot_data to generate spectral 
analysis. User can choose to overplot a best-fit free-free bremsstrahlung 
spectrum, a black-body spectrum, and plot the elements in the visible 
wavelengths. Optional parameters are described at the top of each function.
The best-fit equations are found by looping over the known range of stellar
temperatures and finding the smallest chi-square (best fit) value for the data.

3. Find local minima in the spectrum to get the lines of absorption. Minima
is defined by a range of wavelengths to check and a depth which can
optionally be specified by the user. Plot the entire data set with yellow
absorption lines overlayed.

4. Map absorption lines to common elements / molecules in stellar spectra.
A. create an empty dynamic array of present elements.
B. Create a table (array of arrays) for common elements and their
wavelengths.
C. Loop through each element. Inside, Loop over the object's 
wavelength array. If the object has a wavelength (w +/- dw) close
to an element's wavelength, then add the element to the 
object's element array.
D. For molecules like TiO, define a range of absorbed wavelengths and if there
is an absorption line in this range, mark it as a TiO line.
E. Plot a zoomed-in area of the spectra with dashed lines indicating the 
elements/molecules present in the atmosphere.

6. Plot the visible spectrum if desired.
A. start with a rainbow emission spectra
B. For all of the known absorbed wavelengths, draw an absorption line with 
tick marks for their elements/molecules.

7. Outpt what the stellar temperature, type, and atmospheric abundances 
are expected to be using Wein's Law.
"""

import numpy as np
import pylab as pl
import os
import matplotlib.pyplot as plt

#clear plots
plt.close('all')

#constants
kb = np.float64(1.38*(10**-23)) #boltzmann constant in J/K, or "k"
h = np.float64(6.626*(10**-34))#plank's constant
c = np.float64(3*(10**8))#speed of light
sbc = np.float64(5.67*(10**-8)) #stefan-boltzman constant
b = 2.898*(10**-3) #Weins'd displacement constant: 2.8977721(26)×10−3 m K
    
#dictionaries
def wavelength_to_el():
    #return a mapping (dictionary) of elements and wavelengths in angstroms:
    all = {'Al III': np.array([1670, 1855, 1863]),
           'C II': np.array([1657]),
           'C II': np.array([1335]),
           'C III': np.array([1247, 5696]),
           'C IV': np.array([1540, 1648, 1668]),
           'Ca I': np.array([4308]),
           'Ca II': np.array([1650, 1840, 3968, 3934]), 
           'CH': np.array([4300]),
           'Fe I': np.array([5270, 4958, 4046, 4668, 4384, 3820, 3581, 3021]),
           'Fe II': np.array([1602, 1610]),
           'Fe III': np.array([1896, 1914, 1926]),
           'G-band': np.array([4250]),
           'H I': np.array([1026, 1216]),
           'H-alpha': np.array([6563]), 
           'H-beta': np.array([4861]), 
           'H-gamma': np.array([4341]), 
           'H-delta': np.array([4102]), 
           'H-epsilon': np.array([3970]), 
           'H-zeta': np.array([3889]), 
           'H-eta': np.array([3835]), 
           'H2O': np.array([7165]),
           'He I': np.array([4200, 5876, 584, 10830]),
           'He II': np.array([4400, 4685, 1640]),
           'Hg': np.array([5461]),
           'Mg I': np.array([5170, 5184]),
           'Mg II': np.array([2803, 2796]),
           'MgH': np.array([4800, 5150]),
           'Na I': np.array([5890, 5896]),
           'N III': np.array([1748, 1751, 4634, 4642]),
           'N IV': np.array([1718]),
           'N V': np.array([1240]),
           'Ni': np.array([2994]),
           'O III': np.array([5007, 4959]),
           'O2': np.array([7594, 6867, 8988, 8227, 6277]),
           'Si II': np.array([1808, 1817, 1818]),
           'Si III': np.array([1300]),
           'Si IV': np.array([1260, 1385, 1403, 1425, 1523]),
           'Ti II': np.array([1906, 1908, 3361]),
           }  
    return all
    
def TiO(wl, I):
    #wl in angstrom, I in arbitrary flux/angstrom
    #Returns range of known TiO absorption ranges if an absorption line is 
    #present in that range.
    
    #return empty list if wavelength range does not encompass TiO
    if (min(wl) > 4940 or max(wl) < 6990): return np.array([[]])
    
    #TiO regions
    abs_lines = np.array([[4950, 5200],[5600,5700],[6150,6250],[6650,6980]])
    i1 = (np.abs(wl-4950)).argmin()
    i2 = (np.abs(wl-6980)).argmin()
    found_regions = [[]]
    
    dwl = wl[1]-wl[0] #delta wl in angstroms
    pad = int(50/dwl) #want padding to be dw = 50 Angstroms
    dI = .04*max(I) #if TiO region is at least 4% lower than the surrounding data, 
    #we probably have a bunch of TiO lines
    for x in range(len(abs_lines)):
        start = abs_lines[x,0]
        stop = abs_lines[x,1]
        #find nearest index
        i1 = (np.abs(wl-start)).argmin()
        i2 = (np.abs(wl-stop)).argmin()
        #take the averages of the surrounding area and the TiO area and subtract
        Iav_TiO = np.average(I[i1:i2])
        Iav_around = (np.average(I[i1-pad:i1])+np.average(I[i2:i2+pad]))/2.0
        if ((Iav_around - Iav_TiO) > dI): 
            found_regions.append([start, stop])
    return np.array(found_regions) #change from list to array
    
#MAIN ANALYSIS FUNCTION THAT CALLS OTHER FUNCTIONS    
def plot_data(wl, I, star, ffbs=False, bbs=False, err=10.0, plot=False, vis=False, thresh=5, depth=.1):
    #wl in angstrom, I in arbitrary flux/angstrom
    #star is a string of the spectral type
    #By default, just plot spectra and absorpiton lines with matching elements.
    #If desired, set ffbs=true to get a free-free bremsstrahlung fit.
    #If desired, set bbs=true to get a blackbody fit.
    #Set err to the error (in angstroms) of the wavelengths. This is set to 10 
    #by default since the data sets I'm using have a 10 Angstrom error.
    #Set plot to plot the data to the screen.
    #Set vis to plot the visible spectrum to the screen.
    #set thresh and depth for the width and depth ratio desired for detecting
    #the absorption lines.
    
    print "Plotting Spectra for a ",star,"-type star"
    print ""
    
    #set up figures with:
    #ax1: plot whole data set with yellow absorption lines
    #ax2: plot zoomed-in data set around detected absorption lines and their
    #matching elements/compounds.
    fig, axes = plt.subplots(ncols=2, nrows=1, figsize=(14,5))
    ax1, ax2 = axes.ravel()
    ax3 = ax2.twiny() #to add in custom x-ticks for the elements
    plt.setp(ax3.get_xticklabels(), rotation='vertical', fontsize=8)
    
    #plot data in red
    ax1.plot(wl, I, color='r', label=star+' data')
    ax1.set_ylabel('Spectral Flux Density (arbitrary flux/Angstrom)')
    ax1.set_xlabel('Wavelength (Angstroms)')
    ax1.set_title('Spectrum of a '+star+'-type star')
    ax2.plot(wl, I, color='r')
    ax2.set_ylabel('Spectral Flux Density (arbitrary flux/Angstrom)')
    ax2.set_xlabel('Wavelength (Angstroms)')
    
    #if Free-free bremsstrahlung line-of-best-fit is desired, plot it in blue:
    if (ffbs): ax1.plot(wl, best_fit(wl, I, 'ffbs'), color='b', label='FFBS best fit')
    #if blackbody line-of-best-fit is desired, plot it in green:
    if (bbs): ax1.plot(wl, best_fit(wl, I, 'bbs'), color='g', label='BBS best fit')

    #find strong absorption lines
    abs_lines = strong_abs_lines(wl, I, thresh=thresh, depth=depth)
    print "found absorption lines at wavelengths (Angstroms): ",wl[abs_lines]
    
    #find the tricky TiO absorption regions
    TiO_lines = TiO(wl, I)
    print "TiO lines found in wavelength regions: ",TiO_lines
    
    #get a dictionary of common absorption lines mapped to their element
    abs_lines2 = wavelength_to_el()
    #set tick mark arrays for the elements
    xticks_loc = []
    xticks_els = []
    #matches is the number of matches it found between absorption lines and 
    #elements
    matches = 0
    
    #plot and print absorption lines:
    #offset is the angstrom offset of the abs. line from the known value
    print "Wavelength +/-",err," Ang     Element"
    for i in range(len(abs_lines)): 
        #plot ALL absorption lines in data as YELLOW
        x = wl[abs_lines[i]]    
        if (i == 0): ax1.plot((x, x), (0, max(I)*1.2), 'y-', linewidth = 1, alpha=0.7, label='Abs. line')
        else: ax1.plot((x, x), (0, max(I)*1.2), 'y-', linewidth = 1, alpha=0.7)
        ax2.plot((x, x), (0, max(I)*1.2), 'y-', linewidth = 2)
        for el, y in abs_lines2.items():
            #plot MATCHING absorption lines in DASHED BLACK
            for w in y:
                if (abs(x-w) < err):
                    print "      ",x,"              ",el
                    #only overplot on the zoomed-in data
                    if (matches == 0): ax2.plot((w, w), (0, max(I)*1.2), 'k--', label='Matched Abs. Line')
                    else: ax2.plot((w, w), (0, max(I)*1.2), 'k--')
                    #save the found elements to an array
                    xticks_loc.append(w)
                    xticks_els.append(el)
                    matches += 1
                    
    #separate loop for TiO lines (cleaner)
    for j in range(1,len(TiO_lines)):
        #if found TiO regions, mark the absorption lines
        for i in range(len(abs_lines)): 
            x = wl[abs_lines[i]]
            #if absorption line is in the TiO range, plot and log it
            if ((x >= TiO_lines[j][0]) and (x <=TiO_lines[j][1])):
                print "      ",x,"              ",'TiO'
                ax2.plot((x, x), (0, max(I)*1.2), 'k--')
                xticks_loc.append(x)
                xticks_els.append('TiO')
                matches += 1
                    
    ax1.legend(prop={'size':10})
    ax2.legend(prop={'size':10})
    #draw tick marks on a new axis overplotting on ax2 to display elements
    ax3.set_xticks(xticks_loc)
    ax3.set_xticklabels(xticks_els)

    #number of matches found
    print "found ",matches," matches out of ",len(abs_lines)," absorption lines"
    print ""
    
    #reformat plots
    ax1.set_ylim([0, max(I)*1.2])
    xlim_lo = wl[abs_lines[0]]
    xlim_hi = wl[abs_lines[len(abs_lines)-1]]
    xlim_len = xlim_hi-xlim_lo
    xlim_del = 0.1*xlim_len
    xlim_lo = xlim_lo - xlim_del
    xlim_hi = xlim_hi + xlim_del
    ax2.set_ylim([0, max(I)*1.2])
    ax2.set_xlim([xlim_lo,xlim_hi])
    ax3.set_xlim([xlim_lo,xlim_hi])
    
    #save and display
    plt.savefig(star+"_spectrum.jpg")
    if (plot): plt.show()
    
    #draw visible spectrum
    vis_spec(xticks_loc, xticks_els, star, vis=vis)
    
    #find temperature and determine spectral type
    temp = weins_law(wl, I)
    print "Estimated spectral type: ",spectral_type(temp)
    atmosphere(temp)
    print "True spectral type: ",star
    print "----------------------------------------"
    
#SUB/HELPER FUNCTIONS
def vis_spec(xticks_loc, xticks_els, star, vis=False):
    #xticks_loc maps to xticks_els with wavelengths (Ang) and elements (string)
    #star is the star's spectral type
    #If vis is set to true, a visible spectrum will be output to the screen.
    
    #Define blue and red wavelengths in angstroms.
    #Create a color bar spanning these values.
    #this gets the sodium doublet in the yellow-almost-orange range.
    blue = 4000.0
    red = 6800.0
    a = np.array([[blue,red]])
    
    #if star has no absorbed wavelengths in the visible spectrum, just return
    vis_exists = False
    for i in range(len(xticks_loc)):
        if ((xticks_loc[i] > blue) and (xticks_loc[i] < red)): 
            vis_exists = True
            break
    if (vis_exists == False): 
        print "No absorption lines found in visible wavelengths."
        return False
    
    #set figure size. 
    #Colorbar needs a plot to attach to by default so just clear the plot:
    pl.figure(figsize=(9, 3))
    img = pl.imshow(a, cmap="jet")
    pl.gca().set_visible(False)
    
    #set position of axes:
    h_in = 0.6 #height
    h_px = h_in*212 #convert height to pixels
    cax = pl.axes([0.1, 0.2, 0.8, h_in]) #left, bottom, width, height
    xticks_loc2 = np.array(xticks_loc)

    #draw color bar with ax4
    pl.colorbar(orientation='horizontal', cax=cax, ticks=xticks_loc2)
    plt.setp(cax.get_xticklabels(), fontsize=8, rotation=45)
    cax.set_title("Elements Present in a "+star+"-star's Visible Spectrum", y=1.2)
    ax4 = cax.twiny()
    ax4.set_xticks(xticks_loc2)
    ax4.set_xticklabels(xticks_els, rotation=45)
    plt.setp(ax4.get_xticklabels(), fontsize=8)
    
    #draw a transparent gray line where the elements were found for aesthetics
    ax4.xaxis.set_tick_params(width=4, size=h_px, color=(0,0,0,.3))
    ax4.set_xlabel("Wavelength (Angstroms)")
    ax4.xaxis.labelpad = 30
    ax4.xaxis.set_label_position('bottom') 
    ax4.set_xlim([blue,red])
    
    #over-plot darker lines at matching elements/molecules:
    ax5 = cax.twiny()
    ax5.set_xticks(xticks_loc2)
    ax5.set_xticklabels([]) #no labels
    ax5.xaxis.set_tick_params(width=1, size=h_px, color=(0,0,0,.8))
    ax5.set_xlim([blue,red])

    #show and plot
    if (vis): plt.show()
    pl.savefig(star+"_vis.jpg")
    
def spectral_type(t):
    #returns a string of the spectral type and color based on temperature in K
    if t > 30000: return 'blue O'
    if t > 10000: return 'blue-white B'
    if t > 7500: return 'white A'
    if t > 6000: return 'yellow-white F'
    if t > 5000: return 'yellow G'
    if t > 3500: return 'yellow-orange K'
    if t < 3500: return 'red M'
    
def atmosphere(t):
    #returns spectral type and expected composition based on temp in K
    print "Expected Composition of Stellar Atmosphere: "
    if t > 30000:  print "  O: He+, O++, N++, Si++, He, H"
    elif t > 10000:print "  B: He, H, O+, C+, N+, Si+, C IV, SI IV"
    elif t > 7500: print "  A: H(strongest), Ca+, Mg+, Fe+, some ionized metals"
    elif t > 6000: print "  F: H(weaker), Ca+, Fe+, ionized metals"
    elif t > 5000: print "  G: H(weaker), Ca+, ionized & neutral metal"
    elif t > 3500: print "  K: Ca+(strongest), Na, neutral metals (strong), H(weak)"
    elif t < 3500: print "  M: Strong neutral atoms, Na, molecules, TiO"
    
def weins_law(wl_ang, I):
    #wl_ang is wavelength in angstroms, I in arbitrary flux/angstrom
    wl = wl_ang/(10**10)
    max_I = max(I)
    ind = np.where(I == max_I)
    max_wl = wl[ind]
    print "Max wavelength: ",max_wl," m"
    temp = b/(max_wl)
    print "Stellar Temperature: ",temp," K"
    return temp
    
#define spectral radiance / plank's function:
def spec_rad(w, st):
    exp = np.float64(np.exp(h*c/(w*kb*st)))
    num = np.float64(2*h*(c**2))
    val = np.float64(num/((w**5)*(exp-1)))
    return val
    #W*sr^-1*m^-3.
    
#Return a best-fit free-free bremsstrahlung spectrum OR blackbody spectrum
def best_fit(wl_ang, I, fit='ffbs'):
    #wl_ang is a wavelength array in angstroms
    #I in arbitrary flux/angstrom
    #fit is either 'ffbs' or 'bbs' in flux/wavelength
    
    if (fit == 'bbs'): print "Overplotting a black-body spectrum"
    elif (fit == 'ffbs'): print "Overplotting a free-free bremsstrahlung spectrum"
    else: 
        print "fit type unkown. Use either 'ffbs' or 'bbs'. Returning..."
        return False
    
    #set-up
    wl = wl_ang/(10**10)
    N = len(wl)
    dof = N-1
    I2 = np.arange(N, dtype=np.float) 
    M = 200
    trytemps = np.linspace(2000,80000,num=M) 
    s_min = 1*(10**30)
    best_T = 0
    best_fit = np.arange(N, dtype=np.float64)
    
    #loop through temperatures to find smallest chi-square
    for t in range(M):
        for i in xrange(N): 
            #both in flux/wavelength, like the data
            if (fit == 'ffbs'): I2[i] = np.exp(-h*c/(wl[i]*kb*trytemps[t]))/(wl[i]**5)
            elif (fit == 'bbs'): I2[i] = spec_rad(wl[i], trytemps[t])*4*np.pi
        I2_norm = I2*(max(I)/max(I2))
        chi_squared = 0
        for i in range(N):
            Ok = I[i] #represents count n that falls in bin k based on observed data
            Ek = I2_norm[i] #expected n in bin k
            term = ((Ok-Ek)**2)/Ek #term to sum in chi-squared
            chi_squared = chi_squared + term
        if chi_squared < s_min: s_min, best_T, best_fit = chi_squared, trytemps[t], I2_norm
    
    print "Best fit temperature based on S_min:"
    print "  T: ",best_T
    print "  S_min: ",s_min
    print "  S_min/dof: ",s_min/dof
    print ""
    return best_fit 
        
def strong_abs_lines(wl, I, thresh=5, depth=.1):
    #wl in angstrom, I in arbitrary flux/angstrom
    #optionally a local-minima threshhold (in units the same as the wavelength) 
    #and line depth scale, return an array of the absorbed wavelengths
    
    N = len(wl)
    lines = []
    
    #find local minima. 
    #For element w, if the previous w-thresh elements were all >w, and the next 
    #w+thresh elements are all <w, and the line has a proper depth from the 
    #endpoints of the threshhold, then found an absorption line!
    d = depth*max(I) #min depth of line
    for w in range(thresh, N-thresh):
        is_w_a_min = 1 #assume the value at w is a min unless proven wrong:
        #check shallowness and local minima:
        for dw in range(w-thresh, w+thresh):
            if (I[dw] < I[w]): is_w_a_min = 0
        #check depth:
        if ((I[w-thresh]-I[w] < d) and (I[w+thresh]-I[w] < d)): is_w_a_min = 0
        if(is_w_a_min == 1): lines.append(w)
    return lines

#MAIN PROGRAM
def main():
    #main program - here is where the user will want to specify optional values,
    #directory paths, and file names
    
    #change directories, define stellar types as given in the file names, and plot!
    os.chdir('C:/Users/Sierra/Documents/A_SCHOOL_7/sem10-spring2015/ASTR3800-AstrDataAnalysis/ASTR3800/FINAL/')
    #this is all the data files in this directory:
    #stars = np.array(['O8iii','O5v','B0v','B0i','B8i','A0i','A7iii','A0iii','F0i','rF6v','G0iii','wG5v','rK2iii','K1iv','M1v'])
    #these are some of the more interesting plots:
    stars = np.array(['B8i','F0i','G0iii','A7iii','rK2iii','K1iv'])
    print "----------------------------------------"
    for i in range(len(stars)):
        fname = 'uk'+stars[i]+'.dat'
        data = np.genfromtxt(fname,dtype=float,skip_header=3).T #angstroms
        wl = data[0] #angstroms of wavelength
        I = data[1] #(arbitrary flux/Angstrom)
        N = len(wl)
        #change the default values if desired!
        #plot_data(wl, I, star, ffbs=false, bbs=false, err=10.0, plot=false, vis=false, thresh=5, depth=.1)
        plot_data(wl, I, stars[i], ffbs=True, bbs=True, thresh=10, depth=.085)
    
if __name__ == "__main__":
    main()