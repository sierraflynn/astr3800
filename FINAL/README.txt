Sierra Flynn
ASTR 3800 Final Project

Analysis of Stellar Spectra

---SUMMARY---

My final project reads in stellar spectra (wavelength and spectral flux). 
It plots the data, overplots a free-free bremsstrahlung spectrum, and overplots 
a black-body spectrum. 
It then finds the absorption lines, maps them to elements/molecules, and draws 
the visible absorption spectrum. 
Finally it outputs absorbed wavelengths, matching elements/molecules, expected 
temperature, expected stellar type, and expected atmospheric composition to the 
screen.

---DATA---
The data can be found in .dat files on the following websites:
http://www.eso.org/sci/facilities/paranal/decommissioned/isaac/tools/lib.html
http://www.cadc-ccda.hia-iha.nrc-cnrc.gc.ca/staging/proc/tmp/www/YorkExtinctionSolver/fortran_bin/spectra/stars/

"
The database consists of 131 stellar spectra published by A. Pickles 
(A.J. Pickles, PASP 110,863, 1998)...

The first column is wavelength in
Angstroms, always 1150 - 10620A in steps of 5A. The second column is
F(lambda) for the relevant spectrum, normalised to unity at 5556A.
The spectral range is 1150-25000 Anstroms, sampling 5 Angstroms.
Fluxes are in F_lambda units [normalised to unity at 5556A, in arbitrary units 
per Angstrom: flux/angstrom]. The third column is the rms flux error on the same 
scale. Columns 4-N (Nmax=10) contain the components used, in no particular order.
[Only columns 1 and 2 will be used in my program.] F_lambdas are set to zero 
outside their valid regions. They were then combined using a single pass 
rejection scheme to reject a few outlying pixels. Most bad data was explicitely 
set zero to avoid contamination...

All spectra were formed by combining data from several publicly available
digital libraries of observed stellar spectra, obtained from the
Astronomical Data Centers at Goddard (ADC), or Strasbourg (CDS).
"

---REFERENCES---

visible spectrum for BAFGKM stars:
http://www.skyandtelescope.com/wp-content/uploads/Spectra_L.jpg

Element/molecule mappings to wavelengths and common atmospheric elements/
molecules per spectral type:
http://skyserver.sdss.org/dr2/en/proj/advanced/spectraltypes/lines.asp
http://hyperphysics.phy-astr.gsu.edu/hbase/starlog/staspe.html
http://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/19930022548.pdf
http://en.wikipedia.org/wiki/Fraunhofer_lines
http://vplapps.astro.washington.edu/vplrangemicro
http://www.laserstars.org/data/stars/index.html
http://iopscience.iop.org/0004-637X/570/2/514/pdf/0004-637X_570_2_514.pdf
http://faculty.pepperdine.edu/dgreen/Nasc109/Resources/HR/classification_of_stars.htm