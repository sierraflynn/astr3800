import numpy as np
import os
import matplotlib.pyplot as plt

#read in and save data
os.chdir('C:/Users/Sierra/Documents/A_SCHOOL_7/sem10-spring2015/ASTR3800-AstrDataAnalysis/hw6/')
data = np.genfromtxt('rays.txt',dtype=float).T
x = data[0]
y = data[1]
z = data[2]
qx = data[3]
qy = data[4]
qz = data[5]

#statistics
xmean = np.mean(x)
ymean = np.mean(y)
xdev = x-xmean
ydev = y-ymean

#plot
plt.plot(xdev, ydev, '.')
plt.ylabel("Deviation in Y")
plt.xlabel("Deviation in X")
plt.title("Sierra Flynn's Deviation Plot")
plt.savefig("hw6plot.png")

#truncate
xnew = x[0:25]
ynew = y[0:25]
znew = z[0:25]
qxnew = qx[0:25]
qynew = qy[0:25]
qznew = qz[0:25]
xdevnew = xdev[0:25]
ydevnew = ydev[0:25]

#print a new data file of columns, comma-separated so excel can read it in later
newfile = open('rays_av.txt','wb')
for i in xrange(0,25):
    newfile.writelines([str(xnew[i]),",",str(ynew[i]),",",str(znew[i]),",",str(qxnew[i]),",",str(qynew[i]),",",str(qznew[i]),",",str(xdevnew[i]),",",str(ydevnew[i]),"\n"])
newfile.close()