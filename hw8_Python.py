"""
I have pulled an image of M33 off the NASA Extragalactic Database. It was 
originally in FITS format. In order to open fits you must have access to an 
astronomy user's libraray. Getting access to that library of routines would be 
a hassle for many of us right now. So I opened up the image myself and saved it 
as an unformatted binary file called m33.dat.

Download the file from this site. The image is a 1000x1000 array of integers. 
Write a python procedure to open it read it into an array called 'image'. Plot 
the image to the screen. Change color tables to another one you like....
"""
import numpy as np
#import scipy as sp
#from scipy.stats.stats import pearsonr
import os
import matplotlib.pyplot as plt
import time

#read in and save data
#reference: http://matplotlib.org/users/image_tutorial.html
os.chdir('C:/Users/Sierra/Documents/A_SCHOOL_7/sem10-spring2015/ASTR3800-AstrDataAnalysis/hw8/')
plt.clf()
image = np.fromfile('m33.dat', dtype='int16', sep="")
image = np.array(image.reshape([1000, 1000]))
plt.imshow(image)
plt.title("M33")
plt.savefig("M33.jpg")
plt.show()
time.sleep(5) # delays for 5 seconds

#Possible values are: Spectral, summer, coolwarm, Wistia_r, pink_r, Set1, Set2, Set3, brg_r, Dark2, prism, PuOr_r, afmhot_r, terrain_r, PuBuGn_r, RdPu, gist_ncar_r, gist_yarg_r, Dark2_r, YlGnBu, RdYlBu, hot_r, gist_rainbow_r, gist_stern, PuBu_r, cool_r, cool, gray, copper_r, Greens_r, GnBu, gist_ncar, spring_r, gist_rainbow, gist_heat_r, Wistia, OrRd_r, CMRmap, bone, gist_stern_r, RdYlGn, Pastel2_r, spring, terrain, YlOrRd_r, Set2_r, winter_r, PuBu, RdGy_r, spectral, rainbow, flag_r, jet_r, RdPu_r, gist_yarg, BuGn, Paired_r, hsv_r, bwr, cubehelix, Greens, PRGn, gist_heat, spectral_r, Paired, hsv, Oranges_r, prism_r, Pastel2, Pastel1_r, Pastel1, gray_r, jet, Spectral_r, gnuplot2_r, gist_earth, YlGnBu_r, copper, gist_earth_r, Set3_r, OrRd, gnuplot_r, ocean_r, brg, gnuplot2, PuRd_r, bone_r, BuPu, Oranges, RdYlGn_r, PiYG, CMRmap_r, YlGn, binary_r, gist_gray_r, Accent, BuPu_r, gist_gray, flag, bwr_r, RdBu_r, BrBG, Reds, Set1_r, summer_r, GnBu_r, BrBG_r, Reds_r, RdGy, PuRd, Accent_r, Blues, autumn_r, autumn, cubehelix_r, nipy_spectral_r, ocean, PRGn_r, Greys_r, pink, binary, winter, gnuplot, RdYlBu_r, hot, YlOrBr, coolwarm_r, rainbow_r, Purples_r, PiYG_r, YlGn_r, Blues_r, YlOrBr_r, seismic, Purples, seismic_r, RdBu, Greys, BuGn_r, YlOrRd, PuOr, PuBuGn, nipy_spectral, afmhot
plt.clf()
image2 = plt.imshow(image).set_cmap('hot')
plt.title("M33 (hot)")
plt.savefig("M33hot.jpg")
plt.show()
time.sleep(10) # delays for 5 seconds

"""
Save both images as a jpg's.

Make a contour plot that shows the galaxy. Save that as a jpg.
"""
#http://matplotlib.org/examples/pylab_examples/contour_demo.html
plt.clf()
CS = plt.contour(image, 5)
plt.clabel(CS, inline=1, fontsize=10)
plt.title('M33 Contour Plot')
plt.savefig("M33contour.jpg")
plt.show()
time.sleep(5) # delays for 5 seconds
"""
Then plot a line of intensities across the image. image[*,500]

Save that plot as a jpg.
"""
#print image.shape
plt.clf()
image4 = image[:,500]
#plt.imshow(image)
plt.plot(image4)
plt.title('M33 Intensities at y = 500')
plt.savefig("M33intensities.jpg")
plt.show()
time.sleep(5) # delays for 5 seconds
"""
Find the brightest point in the image. How bright is it? Where is it?

What is the typical brightness per pixel of the galaxy near its center?
"""
#brightness: or whiteness. White has the maximum value.
maxbright = np.max(image)
avg_galaxy_bright = 0
print "max brightness is ",maxbright," at indeces x,y: "
for i in xrange(0,1000):
    for j in xrange(0,1000):
        if image[i, j] == maxbright: print [i,j]
        
#center is at 500,500. 
for i in xrange(450,550):
    for j in xrange(450,550):
        avg_galaxy_bright = avg_galaxy_bright+image[i,j]
        
avg_galaxy_bright = avg_galaxy_bright/(100**2)
print "average galaxy brightness: ",avg_galaxy_bright
#maxbrighti = image.index(maxbright)
plt.close()


"""
Email the answers to last to two lines to Baylee, along with the images as 
attachments to get credit for the homework.
"""