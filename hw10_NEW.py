# -*- coding: utf-8 -*-
import numpy as np
import os
import matplotlib.pyplot as plt
from scipy.stats import chi2

#clear plots
plt.close('all')

#constants:
kb = np.float64(1.38*(10**-23)) #boltzmann constant in J/K, or "k"
h = np.float64(6.626*(10**-34))#plank's constant
c = np.float64(3*(10**8))#speed of light
sbc = np.float64(5.67*(10**-8)) #stefan-boltzman constant
b = 2.898*(10**-3) #Weins'd displacement constant: 2.8977721(26)×10−3 m K

#define spectral radiance / plank's function:
def spec_rad(w, st):
    exp = np.float64(np.exp(h*c/(w*kb*st)))
    num = np.float64(2*h*(c**2))
    val = np.float64(num/((w**5)*(exp-1)))
    return val
    #W*sr^-1*m^-3.

def main():
    
    print ""
    print "ACTUAL DATA: "
    print ""
    
    #read in and save data
    os.chdir('C:/Users/Sierra/Documents/A_SCHOOL_7/sem10-spring2015/ASTR3800-AstrDataAnalysis/hw10/')
    data = np.genfromtxt('poissspec.txt',dtype=float,skip_header=1).T
    wl = data[0]*(10**-10) #m
    ct = data[1] #counts
    N = len(wl)
    dof = N-1.0

    #First, fit a blackbody spectrum. Find S_min. What is the best fit temperature? 
    #Is this a good model fit?
    
    #1 Wein's Law:
    max_ct = max(ct)
    ind = np.where(ct == max_ct)
    max_wl = wl[ind] #angstroms of max wavelength
    print "Max data wavelength: ",max_wl," m"
    temp = b/(max_wl)
    print "Expected temperature based on maximum wavelength of actual data: ", temp
    
    print ""
    print "BEST FIT BASED ON BLACKBODY SPECTRUM: "
    print ""
    
    #to get a best fit using the BB spectrum, need to try a bunch of 
    #temperatures to get the smallest chi-squared value.
    M = 1000
    trytemps = np.linspace(7000000,8000000,num=M)
    s_min = 1*(10**30)
    best_T = 0
    best_fit = np.arange(N, dtype=np.float64)
    fit = np.arange(N, dtype=np.float64)
    
    for t in range(M):
        #Use Planck's function for spectral radiance given in W*Sr^-1*m^-3
        #Convert to intensity (W/m^2) by multiplying by steradians and wavelength
        for i in xrange(N): 
            fit[i] = spec_rad(wl[i], trytemps[t])*wl[i]*4*np.pi
        #find max value of fit and scale:
        fit_norm = fit*(max(ct)/max(fit))
        chi_squared = 0.0
        for i in range(N):
            Ok = ct[i] #represents count n that falls in bin k based on observed data
            Ek = fit_norm[i] #expected n in bin k
            term = ((Ok-Ek)**2)/Ek #term to sum in chi-squared
            chi_squared = chi_squared + term
        if chi_squared < s_min: s_min, best_T, best_fit = chi_squared, trytemps[t], fit_norm
        
    print "Best fit temperature and S_min based on changing T alone:"
    print "  T: ",best_T
    print "  S_min: ",s_min
    print "  S_min/dof: ",s_min/dof
    
    ax1 = plt.gca()
    ax2 = ax1.twinx()
    ax2.plot(wl,best_fit)
    width = 0.2*10**-10       # the width of the bars
    rects1 = ax1.bar(wl, ct, width, color='r') 
    ax1.set_ylabel('Counts')
    ax1.set_xlabel('Wavelength (m)')
    ax1.set_title('BB Spectrum fit to Counts of Wavelengths')
    plt.savefig("hw10_bbspec_NEW.jpg")
    plt.show()

    #----------------------------------------------------------
    
    print ""
    print "BEST FIT BASED ON FREE-FREE BREMSSTRAHLUNG SPECTRUM: "
    print ""
    
    #Then try a free-free bremsstrahlung spectrum (j d_nu = exp(-h*nu/k/T) d_nu). 
    #What is the best fit temperature? Is this a significantly better fit than a 
    #blackbody? Is this a good model fit? 
    ct2 = np.arange(N, dtype=np.float) 
    trytemps = np.linspace(14000000,16000000,num=M) 
    s_min = 1*(10**30)
    best_T = 0
    best_fit = np.arange(N, dtype=np.float64)
    
    for t in range(M):
        for i in xrange(N): 
            ct2[i] = np.exp(-h*c/(wl[i]*kb*trytemps[t]))/(wl[i]**2)
        ct2_norm = ct2*(max(ct)/max(ct2))
        chi_squared = 0
        for i in range(N):
            Ok = ct[i] #represents count n that falls in bin k based on observed data
            Ek = ct2_norm[i] #expected n in bin k
            term = ((Ok-Ek)**2)/Ek #term to sum in chi-squared
            chi_squared = chi_squared + term
        if chi_squared < s_min: s_min, best_T, best_fit = chi_squared, trytemps[t], ct2_norm #I_norm
    
    print "Best fit temperature and S_min based on changing T alone:"
    print "  T: ",best_T
    print "  S_min: ",s_min
    print "  S_min/dof: ",s_min/dof
    
    fig2 = plt.figure()
    ax3 = fig2.gca()
    ax4 = ax3.twinx()
    ax4.plot(wl,best_fit)
    rects3 = ax3.bar(wl, ct, width, color='r')
    ax3.set_ylabel('Counts')
    ax3.set_xlabel('Wavelength (m)')
    ax3.set_title('FFB Spectrum fit to Counts of Wavelengths')
    
    plt.savefig("hw10_ffbspec_NEW.jpg")
    plt.show()
    
    #----------------------------------------------------------
    
    #With the better fit model, estimate the range of temperatures that will 
    #include the "true" temperature, 95% of the time. To do this, create a 100x100 
    #array of S values around S_min in the two free parameters of intensity and 
    #temperature. Make a contour plot of the two-dimensional confidence interval. 
    #The countours should be at 65%, 95% and 99.5% confidence levels. Be certain to 
    #label axes and make it look professional!  
    
    print ""
    print "Since FFBS gave a better fit over BBS, calculate intensities based on FFBS for the countour diagram."
    dT = 5000000
    Tlo = best_T-dT
    Thi = best_T+dT
    temps = np.linspace(Tlo,Thi,num=100) #stellar temps range
    S_values = np.zeros((100,100))  
    
    #if I don't divide X^2 by DOF, get values of 100, 400, 900 (99.7) 
    #for confidence intervals. if DO, get 1, 4, 9.
    
    for t in range(100):
        for i in xrange(N): fit[i] = np.exp(-h*c/(wl[i]*kb*temps[t]))/(wl[i]**2)
        best_fit = fit*(max(ct)/max(fit))
        chi_squared = 0.0
        for i in range(100):
            Ok = ct[i] #actual data representing intensity
            Ek = best_fit[i]
            term = ((Ok-Ek)**2)/Ek #term to sum in chi-squared
            chi_squared = chi_squared + term   
            S_values[t][i] = chi_squared   
    plt.figure()
    
    #cdf = cumulative density function for the X^2. 99 = DOF.
    conf = chi2.cdf(S_values, dof)
    levels = np.array([0.65, 0.95, 0.995])
    del_chi2 = np.diff(S_values)/dof
    #not sure why this isn't plotting the error bars correctly...
    CS = plt.contour(conf, levels, yerr = del_chi2)
    plt.errorbar(conf[0][:], conf[:][0], del_chi2, fmt='bo')
    plt.clabel(CS, inline=1, fontsize=10)
    plt.xlabel('S_values')
    plt.ylabel('Probability Density')
    plt.title('Contour Plot of Confidence Intervals of Chi-Squared Values')
    plt.savefig("hw10_contour_NEW.jpg")
    plt.show()
    
    print "---------------------------------------------"     
        
if __name__ == "__main__":
    main()