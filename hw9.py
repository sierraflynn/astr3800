"""
Homework #9:

Taylor, problems 11.5 and 12.13

Propose a project to be completed by the end of the semester. In one 
paragraph describe what you would like your program to do. In a second 
paragraph briefly describe what your approach to the problem will be. 
Astronomy themes are strongly encouraged, but not completely required. 
Email your project proposal to me for acceptance. Alternatively, wait 
and talk to me in class about your idea.
"""

import numpy as np
#import scipy as sp
#from scipy.stats.stats import pearsonr
import os
import matplotlib.pyplot as plt
import time
import math

#read in and save data
#reference: http://matplotlib.org/users/image_tutorial.html
os.chdir('C:/Users/Sierra/Documents/A_SCHOOL_7/sem10-spring2015/ASTR3800-AstrDataAnalysis/')

#11.5: p. 276. radioactive sample three decays per minute. v decays per 100 1-min 
#intervals. a. make histogram of frac v found vs. v. b. on same plot show P3(v) 
#(expected distribution) 
#Does data fit distribution? could use chi-squared test to determine this.

number = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]) #x
times = np.array([5, 19, 23, 21, 14, 12, 3, 2, 1, 0])
tottimes = 100.
frac = times/tottimes #y

width = 0.35       # the width of the bars
fig, ax = plt.subplots()
rects1 = ax.bar(number, frac, width, color='r')

# add some text for labels, title and axes ticks
ax.set_ylabel('Fraction of 100 Observations')
ax.set_xlabel('Number of Decays in 1 Minute')
ax.set_title('Histogram of Decays in 100 1-Minute Intervals')
ax.set_xticks(number+width)
ax.set_xticklabels(number)

#poisson function for probability of observing v events in time T:
x = range(10)
p = range(10)
for i in range(10):
  p[i] = np.exp(-3)*(3**x[i])/math.factorial(x[i]) #3 is the given mean
  
plt.plot(x, p)
plt.savefig("hw9_histogram_decays.jpg")
plt.show()

#yes, the data seems to fit well!***

#12.13: p. 301. calculate chi-squared for this. assume follows poisson dis.
#with mean = 3. group all values v >= 6 in a single bin. deg of freedom? what is
#chi-squared? matches poisson distrib?

chi_squared = 0
for i in range(7):
    Ok = frac[i] #represents count n that falls in bin k based on observed data
    if i == 6: Ok = frac[6]+frac[7]+frac[8]+frac[9] #group into a single bin
    Ek = p[i] #expected n in bin k based on Poisson
    if i == 6: Ek = p[6]+p[7]+p[8]+p[9]
    print "for bin ",i,", observed: ",Ok,", Expected: ",Ek
    term = ((Ok-Ek)**2)/Ek #term to sum in chi-squared
    chi_squared = chi_squared + term
    
print "If X2 = 0, perfect fit; if X2 ~< n=7, in agreement; if X2 >> n=7, data "
print "does not match distribution. Results: ", chi_squared

#deg of freedom d = n - c = 7 bins - constraints. One constraint is the equation
#for the Poisson distribution. Since the mean is given to us it's not a constraint
#since we didn't calculate it. => d = 7 - 1 = 6.