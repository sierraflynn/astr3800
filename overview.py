#Thurs 1/22 programming overview
import numpy as np

def NR_iter(f, fprime, guess, tol=1e-6, prints=True):
     #This function performs the Newton-Raphson method
     #f = function
     #fprime = its derivative
     #guess = initial solution. 
     #tol = how closely the solution must converge. No user input, use default.
     #prints = optional keyword to print output.
     
     #initial values
     x_n = guess
     x_n1 = x_n - f(x_n)/fprime(x_n)
     
     while np.abs(f(x_n1)) >= tol:
        x_n = x_n1
        x_n1 = x_n - f(x_n)/fprime(x_n) #for next iteration
         
     if prints:
        print(x_n1)
           
     return x_n1
     
#lambda function example
g = lambda x: 3*(np.exp(x)-1)-x*np.exp(x)
gprime = lambda x: 2*np.exp(x)-x*np.exp(x)

#do the computation
value=3.
solution = NR_iter(g, gprime, value, prints=True)

#to run this in terminal, type 'python overview.py'

#bitbucket info:
#cd to dir
#git init
#