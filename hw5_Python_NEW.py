"""
Homework 5. Add-on:
Then convert the spectrum to photon units from energy units by dividing by 
h*nu. Plot the spectrum in units of photons per cm2 per sec in each bin (array 
element) of the spectrum.

Create a telescope by setting the effective area and observation time. Let's 
do Hubble!

HST has an aperture of 2.4m with obscuration of 20% by the secondary mirror. 
The throughput to the spectrograph is about 30%. Assume that the response is 
constant over wavelength for the moment. Assume an observation of 1000second. 
Put the Sun at 1000pc. Plot the expected number of counts (N) per bin.

Loop through the bins of your spectrum. For each bin call dn=random.normal 
(from numpy) and add it to N. Plot the result as a histogram. It should look 
like real data.
"""
import numpy as np
import matplotlib.pyplot as plt
#import os
from scipy.integrate import simps
import copy
#import pylab as pl
#import time
#pl.ion()

#constants:
kb = np.float64(1.38*(10**-23)) #boltzmann constant in J/K
h = np.float64(6.626*(10**-34))#plank's constant
c = np.float64(3*(10**8))#speed of light
sbc = np.float64(5.67*(10**-8)) #stefan-boltzman constant

#stefan-boltzman law, to check:
def stef_boltz_law(st, r, d):
    ratio = (r**2)/(d**2)
    return ratio*sbc*(st**4)

#define spectral radiance / plank's function:
def spec_rad(w, st):
    exp = np.float64(np.exp(h*c/(w*kb*st)))
    num = np.float64(2*h*(c**2))
    val = np.float64(num/((w**5)*(exp-1)))
    return val
    #W*sr^-1*m^-3.
     
#define flux for wavelength w:   
def flux(w, r, d, st):
    #spectral radiance in W/sr/m^2/m. We want flux in W/m^2.
    sa_atr = np.float64(np.pi*(r**2)) #m^2, to cancel out the term in spec_rad
    sa_atd = np.float64(4*np.pi*(d**2)) #m^2, to get the flux at d
    sr = np.float64(4*np.pi) #sr, to cancel out the steradian
    rad_flux = np.float64(spec_rad(w, st)*sr*(sa_atr/sa_atd)) #W/m^3
    val = rad_flux
    return val #W/m^3 for a wavelength
    #integral of W/m^3 should give 1400 for earth.

#plot blackbody function:
def plot_blackbody_flux(name, st, r, d):
    
    #Determine the color of the star:
    if st > 11000: color = 'blue' #blue
    if st < 11000 and st > 7500: color = 'gray' #white
    if st < 7500 and st > 5000: color = 'yellow' #yellow
    if st < 5000 and st > 3600: color = 'orange' #orange
    if st < 3600: color = 'red' #red
    
    #define wavelength and flux arrays:    
    w0 = 50. #angstroms
    w1 = 30000. #angstroms
    size = 1000 #want 1,000 data points
    incr = (w1-w0)/size
    
    flux_arr = np.arange(size, dtype=np.float64)
    w_arr = np.arange(size, dtype=np.float64)
    w_arr[0] = w0
    #fill wavelength array
    for i in range(1, size):
        w_arr[i] = w_arr[i-1]+incr
    w_arr = np.float64(w_arr*(10**-10)) #get to meters
 
    #calculate spectral flux at each wavelength   
    for i in range(0, size):
        flux_arr[i]=flux(w_arr[i],r,d,st)
        
    #find maximum flux at peak wavelength
    maxflux = np.max(flux_arr)
    k = np.where(flux_arr == maxflux)
    peakw = w_arr[k]
    maxflux = maxflux*peakw
    
    #integrate to get total flux:
    tot_flux = simps(flux_arr, w_arr)
    lum = tot_flux*(4*np.pi*d**2)
    sol_lum = 3.846*10**26 #watts
    sbf = stef_boltz_law(st, r, d)
    
    print "-------------------------------------"
    print "Flux Profile for "+name+" :"
    print "Maximum flux at peak wavelength: ",maxflux," W/m^2"
    print "peak wavelength: ",peakw," m"
    print "Max wavelength from Wein's Law: ",0.0029/st," m"
    print "Error: ",(np.abs(peakw-(0.0029/st))/(0.0029/st))*100,"%"
    print "TOTAL FLUX (integrated): ",tot_flux,"W/m^2"
    print "Flux from SB Law: ",sbf,"W/m^2"
    print "Error: ",(np.abs(sbf-tot_flux)/sbf)*100,"%"
    print "Luminosity: ",lum
    print "Solar Luminosities: ",lum/sol_lum
    print "Error: ",(np.abs(lum-sol_lum)/sol_lum)*100,"%"
    
    #Plot flux (W/m^2) vs. wavelength:
    #http://matplotlib.org/examples/style_sheets/plot_ggplot.html
    #fig, axes = plt.subplots(ncols=2, nrows=3)
    #ax1, ax2, ax3, ax4, ax5, ax6 = axes.ravel()
    fig, axes = plt.subplots(ncols=2, nrows=2)
    ax1, ax2, ax3, ax4 = axes.ravel()

    ax1.plot(w_arr,flux_arr*w_arr,c=color,label=name)
    ax1.set_ylabel("Flux (W/m^2)")
    ax1.set_xlabel("Wavelength (m)")
    ax1.set_title("Flux (W/m^2)")
        
    """
    Then convert the spectrum to photon units from energy units by dividing by 
h*nu. Plot the spectrum in units of photons per cm2 per sec in each bin (array 
element) of the spectrum.
"""
    
    #plot in wavelength space***
    ph_E = h*c/w_arr #photon energy in J: (J*s)*(m/s)/m
    ph_counts = (flux_arr*w_arr)/ph_E #(W/m^3)*m/J = counts per m^2 per sec
    ph_counts = ph_counts/(100**2) #counts per cm^2 per sec
    n = 50 #some integer, for wavelengths per bin
    J = len(w_arr)/n #number of bins
    dw = w_arr[1]-w_arr[0] #dist between wavelengths (m)
    Dw = n*dw #bin width in pixels?
    w_cent = np.zeros(J) #central wavelength
    ph_counts_bin = np.zeros(J) #separate into bins; in this case same as ph_counts
    for j in xrange(0,J): w_cent[j] = w_arr[j*n+2]#w_arr[0]+Dw*((w_arr[j-1]+w_arr[j])/2)
    #print "first few wavelengths: ",w_arr[0],w_arr[1],w_arr[2]
    #print "central wavelengths: ",w_cent[0], w_cent[1]
    for j in xrange(0,J):ph_counts_bin[j] = sum(ph_counts[(j*n):((j*n)+(n-1))])
    ax2.bar(w_cent-Dw/2, ph_counts_bin, Dw, color='r') #subtract Dw/2 to move it left
    #ax2.bar(w_arr-Dw/2, ph_counts, Dw, color='r') 
    #flux_arr2 = np.arange(size, dtype=np.float64)
    #for i in xrange(0, size): flux_arr2[i] = flux_arr[i]*w_arr[i]
    #ax2.hist(flux_arr2,bins=20)
    ax2.set_xlabel("Wavelength (m)")
    ax2.set_ylabel("Photon Counts/cm^2/s")
    ax2.set_title("Photons per Area per Sec")
    ax2.set_xlim([0,3*10**-6])
    
    """
    HST has an aperture of 2.4m with obscuration of 20% by the secondary mirror. 
    The throughput to the spectrograph is about 30%. Assume that the response is 
    constant over wavelength for the moment. Assume an observation of 1000second. 
    Put the Sun at 1000pc. Plot the expected number of counts (N) per bin.
    """
    r_tele = 2.4/2 #m
    area_tele = (((np.pi*r_tele**2)*.8)*.3) #m^2 #fixed***
    exposure = 1000. #sec
    #takes obscuration of mirror and true throughput of spectrograph into account
    #note: ph_energy_array curently in phE/cm^2/sec
    #need to multiply by SA of telescope to SA of space sphere
    #and multiply by exposure
    #to get phE/m^2 at Hubble
    #sa_ratio = area_tele#/(4*np.pi*d**2) already an area***
    ph_counts3 = copy.deepcopy(ph_counts)*exposure*area_tele
    #***don't plot this exactly...
    ph_counts3_bin = np.zeros(J) #separate into bins; in this case same as ph_counts
    for j in xrange(0,J):ph_counts3_bin[j] = sum(ph_counts3[(j*n):((j*n)+(n-1))])
    ax3.bar(w_cent-Dw/2, ph_counts3_bin, Dw, color='r') 
    ax3.set_xlabel("Wavelength (m)")
    ax3.set_ylabel("Photon Count (N)")
    ax3.set_title("Photons at Hubble")
    ax3.set_xlim([0,3*10**-6])
    
    """Loop through the bins of your spectrum. For each bin call dn=random.normal 
    (from numpy) and add it to N. Plot the result as a histogram. It should look 
    like real data."""
    
    #plot in wavelength space***
    ph_counts_rand = copy.deepcopy(ph_counts3)#np.zeros(J)#ph_energy_array2**0.5*np.random.normal(size=ph_energy_array2.shape) #np.zeros(J)
    #ph_counts_rand += ph_counts3**0.5*np.random.normal(size=ph_counts3.shape)
    #ax6.bar(w_cent-Dw/2, ph_counts_rand, Dw, color='r') #subtract Dw/2 to move it left
    ph_counts_rand_bin = np.zeros(J) #separate into bins; in this case same as ph_counts
    for j in xrange(0,J):ph_counts_rand_bin[j] = sum(ph_counts_rand[(j*n):((j*n)+(n-1))])
    #print "check photon counts: ",ph_counts_rand_bin[1],ph_counts_rand_bin[5]
    #ph_counts_rand_bin += ph_counts_rand_bin**0.5*np.random.normal(size=ph_counts_rand_bin.shape)
    ph_counts_rand_bin += (ph_counts_rand_bin**0.8)*np.random.normal(size=ph_counts_rand_bin.shape)
    #print "check random photon counts: ",ph_counts_rand_bin[1],ph_counts_rand_bin[5]
    ax4.bar(w_cent-Dw/2, ph_counts_rand_bin, Dw, color='r') 
    ax4.set_xlabel("Wavelength (m)")
    ax4.set_ylabel("Photon Count (N)")
    ax4.set_title("Randomized Photons at Hubble")
    ax4.set_xlim([0,3*10**-6])
    
    #fix formatting
    plt.subplots_adjust(hspace=0.7)
    ax1.xaxis.major.formatter.set_powerlimits((0,0)) 
    ax1.yaxis.major.formatter.set_powerlimits((0,0)) 
    ax2.xaxis.major.formatter.set_powerlimits((0,0)) 
    ax2.yaxis.major.formatter.set_powerlimits((0,0)) 
    ax3.xaxis.major.formatter.set_powerlimits((0,0)) 
    ax3.yaxis.major.formatter.set_powerlimits((0,0)) 
    ax4.xaxis.major.formatter.set_powerlimits((0,0)) 
    ax4.yaxis.major.formatter.set_powerlimits((0,0)) 
    plt.show() 

def main():
    #for the sun from Hubble:
    
    name = "Photon Energies of the Sun as measured from Hubble"
    st = 5800. #surface temp of sun in K
    r = 6.995*(10**8) #radius of sun in m
    #note 1 au = 5*10**-6 pc.
    au = 1.5*(10**11) #m
    #d = np.float64(10.*(au/(5.*10**-6))) #distance to sun in m, = 1 AU
    d = 1000*(3.086*10**16) #m
    plot_blackbody_flux(name, st, r, d)    
    
    #NOTE TO GRADER: To see plots from homework 3 & 4, 
    #for some other stars, see hw3_Python.py and hw4_Python.py.                     
      
if __name__ == "__main__":
    main()
    