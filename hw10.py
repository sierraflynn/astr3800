# -*- coding: utf-8 -*-
import numpy as np
import os
import matplotlib.pyplot as plt

#clear plots
plt.close('all')

#constants:
kb = np.float64(1.38*(10**-23)) #boltzmann constant in J/K, or "k"
h = np.float64(6.626*(10**-34))#plank's constant
c = np.float64(3*(10**8))#speed of light
sbc = np.float64(5.67*(10**-8)) #stefan-boltzman constant

#define spectral radiance / plank's function:
def spec_rad(w, st):
    exp = np.float64(np.exp(h*c/(w*kb*st)))
    num = np.float64(2*h*(c**2))
    val = np.float64(num/((w**5)*(exp-1)))
    return val
    #W*sr^-1*m^-3.

def main():
    
    print ""
    print "ACTUAL DATA: "
    print ""
    
    #read in and save data
    #reference: http://matplotlib.org/users/image_tutorial.html
    os.chdir('C:/Users/Sierra/Documents/A_SCHOOL_7/sem10-spring2015/ASTR3800-AstrDataAnalysis/hw10/')
    data = np.genfromtxt('poissspec.txt',dtype=float,skip_header=1).T
    wl0 = data[0] #angstroms
    ct0 = data[1] #counts
    N0 = len(wl0)
    num_obs = sum(ct0)
    frac0 = ct0/num_obs
    """
    #group in bins of 10 instead of 100
    wl = np.zeros(10)
    frac = np.zeros(10)
    ct = np.zeros(10)
    N = 10
    for i in range(10):
        wl[i] = 0
        frac[i] = 0
        ct[i] = 0
        for j in range(10):
            wl[i] = wl[i]+wl0[i+j]
            frac[i] = frac[i]+frac0[i+j]
            ct[i] = ct[i]+ct0[i+j]
    """
    wl = wl0
    ct = ct0
    N = N0
    frac = frac0
    
    #Analyze the spectrum using an S statistic.
    
    #First, fit a blackbody spectrum. Find S_min. What is the best fit temperature? 
    #Is this a good model fit?
    
    #1 Wein's Law:
    max_ct = max(ct)
    ind = np.where(ct == max_ct)
    max_wl = wl[ind] #angstroms of max wavelength
    print "Max data wavelength: ",max_wl*(10**-10)," m / ",max_wl," Angstroms"
    b = 2.898*(10**-3) #Weins'd displacement constant: 2.8977721(26)×10−3 m K
    temp = b/(max_wl*(10**-10))
    print "Expected temperature based on maximum wavelength of actual data: ", temp
    
    print ""
    print "BEST FIT BASED ON BLACKBODY SPECTRUM: "
    print ""
    
    #to get a best fit using the BB spectrum, need to try a bunch of 
    #temperatures to get the smallest chi-squared value.
    trytemps = np.linspace(2200000,2400000,num=1000) #np.linspace(3000,30000,num=100)
    s_min = 1*(10**30)
    best_T = 0
    best_fit = np.arange(N, dtype=np.float64)
    
    for t in range(1000):
        
        fit = np.arange(N, dtype=np.float64)
        #for i in xrange(0, N-1): best_fit[i] = spec_rad(wl[i]*10**-10, temp) 
        #Use Planck's function for spectral radiance given in W*Sr^-1*m^-3
        #Convert to intensity (W/m^2) by multiplying by steradians and wavelength
        for i in xrange(0, N-1): 
            fit[i] = spec_rad(wl[i]*(10**-10), trytemps[t])*(wl[i]*(10**-10))*np.pi
        
        #find max value of best_fit:
        max_spec_rad = max(fit)
        #ind = np.where(fit == max_spec_rad)
        #max_wl = wl[ind] #angstroms of max wavelength
        #b = 2.898*(10**-3) #Weins'd displacement constant: 2.8977721(26)×10−3 m K
        #temp = b/(max_wl*(10**-10))
        
        #scale best_fit by the ratio of the max_spec_rad and max_wl:
        max_frac = max(frac)
        ratio = (max_frac)/(max_spec_rad) #multiply by 1.1 to make a better fit?***
        new_fit = fit*ratio    
        
        chi_squared = 0
        for i in range(N):
            Ok = frac[i] #represents count n that falls in bin k based on observed data
            Ek = new_fit[i] #expected n in bin k based on Poisson
            #if i == 20: print "for bin ",i,", observed: ",Ok,", Expected: ",Ek
            term = ((Ok-Ek)**2)/Ek #term to sum in chi-squared
            chi_squared = chi_squared + term
        #print "If X2 = 0, perfect fit; if X2 ~< n=100, in agreement; if X2 >> n=100, data "
        #print "does not match distribution. Results: ", chi_squared
        if chi_squared < s_min: s_min, best_T, best_fit = chi_squared, trytemps[t], new_fit
        
    print "Best fit temperature and S_min:"
    print "  T: ",best_T
    print "  S_min: ",s_min
    
    ax1 = plt.gca()
    ax2 = ax1.twinx()
    ax2.plot(wl,best_fit)
    #ax2.set_ylim(0,0.02)
    #ax2.set_ylim(0,2.3*(10**28)) #***rescale to get a line of best fit
    width = 0.2       # the width of the bars
    rects1 = ax1.bar(wl, frac, width, color='r') 
    # add some text for labels, title and axes ticks
    ax1.set_ylabel('Fraction of Total Observations')
    ax1.set_xlabel('Wavelength')
    ax1.set_title('Histogram of Wavelengths')
    ax1.set_xticks(wl+width)
    ax1.set_xticklabels(wl) 
    plt.savefig("hw10_bbspec.jpg")
    plt.show()

    #----------------------------------------------------------
    
    print ""
    print "BEST FIT BASED ON FREE-FREE BREMSSTRAHLUNG SPECTRUM: "
    print ""
    
    #Then try a free-free bremsstrahlung spectrum (j d_nu = exp(-h*nu/k/T) d_nu). 
    #What is the best fit temperature? Is this a significantly better fit than a 
    #blackbody? Is this a good model fit? 
    # d_I = e^(-h*frequency/(k*T)) * d_frequency => amount added to I0
    # => I = (-kT/h)*e^(-hf/kT) + I0
    # c = wavelength*frequency
    freq = c/(wl*10**-10)
    #print "fequencies: ",freq
    #d_freq = freq[1]-freq[0] #wavelength are separated by equal amounts
    #print "d_freq: ",d_freq
    I = np.arange(N, dtype=np.float)
    I0 = 0#ct[0] #initial intensity is proportional to the initial count?***
    """
    for i in xrange(0, N): I[i] = (-kb*temp/h)*np.exp(-h*freq[i]/(kb*temp)) + I0
    max_intensity = max(I)
    ratio = (max_frac)/(max_intensity)
    new_I = ((I*ratio*10**-10)+.1)/5.0 #scale and offset
    
    chi_squared = 0
    for i in range(N):
        Ok = frac[i] #represents count n that falls in bin k based on observed data
        Ek = new_I[i] #expected n in bin k based on Poisson
        #if i == 20: print "for bin ",i,", observed: ",Ok,", Expected: ",Ek
        term = ((Ok-Ek)**2)/Ek #term to sum in chi-squared
        chi_squared = chi_squared + term

    print "Best fit temperature and S_min:"
    print "  T: ",temp
    print "  S_min: ",chi_squared
    """
    
    trytemps = np.linspace(5000000,6000000,num=1000) #np.linspace(3000,30000,num=100)
    s_min = 1*(10**30)
    best_T = 0
    best_fit = np.arange(N, dtype=np.float64)
    
    for t in range(1000):
    
        #for i in xrange(0, N): I[i] = (-kb*temp/h)*np.exp(-h*freq[i]/(kb*temp)) + I0
        for i in xrange(0, N): 
            I[i] = (-kb*trytemps[t]/h)*np.exp(-h*freq[i]/(kb*trytemps[t])) + I0
        max_intensity = max(I)
        ratio = -(max_frac)/(max_intensity)
        new_I = ((I*ratio*10**-10)+.1)/5.0 #scale and offset
        
        chi_squared = 0
        for i in range(N):
            Ok = frac[i] #represents count n that falls in bin k based on observed data
            Ek = np.abs(new_I[i]) #expected n in bin k based on Poisson
            #if i == 20: print "for bin ",i,", observed: ",Ok,", Expected: ",Ek
            term = ((Ok-Ek)**2)/Ek #term to sum in chi-squared
            chi_squared = chi_squared + term
        if chi_squared < s_min: s_min, best_T, best_fit = chi_squared, trytemps[t], new_I
    
    print "Best fit temperature and S_min:"
    print "  T: ",best_T
    print "  S_min: ",s_min
    
    fig2 = plt.figure()
    ax3 = fig2.gca()
    ax4 = ax3.twinx()
    ax4.plot(wl,best_fit)
    #ax4.set_ylim(-4.0*(10**16),+0.5*(10**16)) #***rescale to get a line of best fit
    rects3 = ax3.bar(wl, frac, width, color='r')
    # add some text for labels, title and axes ticks
    ax3.set_ylabel('Fraction of Total Observations')
    ax3.set_xlabel('Wavelength')
    ax3.set_title('Histogram of Wavelengths')
    ax3.set_xticks(wl+width)
    ax3.set_xticklabels(wl)
    
    plt.savefig("hw10_ffbspec.jpg")
    plt.show()
    
    #With the better fit model, estimate the range of temperatures that will 
    #include the "true" temperature, 95% of the time. To do this, create a 100x100 
    #array of S values around S_min in the two free parameters of intensity and 
    #temperature. Make a contour plot of the two-dimensional confidence interval. 
    #The countours should be at 65%, 95% and 99.5% confidence levels. Be certain to 
    #label axes and make it look professional!    
    
    temps = np.linspace(5000000,6000000,num=100) #stellar temps range from 3k - 30k
    intensities = frac #np.linspace(min(frac),max(frac),num=100)
    S_values = np.zeros((100,100))#np.arange([100,100], dtype=np.float)
    #chi_squared = 0.0
    for t in range(100):
        chi_squared = 0.0
        for i in range(100):
            Ok = frac[i] #actual data representing intensity
            Ek = (-kb*temps[t]/h)*np.exp(-h*freq[i]/(kb*temps[t])) + I0 
            ratio = -(max_frac)/(Ek)
            Ek = ((Ek*ratio*10**-10)+.1)/5.0 #scale and offset
            term = ((Ok-Ek)**2)/Ek #term to sum in chi-squared
            chi_squared = chi_squared + term   
            S_values[t][i] = chi_squared   
            #print S_values[t][i]  
            #based on free variable temperature
            #this is using the FFBS intensity equation
            #intensities[i] #best fit model
    #plt.clr()
    plt.figure()
    #deg of freedom: 1 type of observed data, 1 param computed
    #*** probably should have grouped in bins of 10 or so...
    #Note for 1 deg of freedom, probabilities (p. 293):
    #99%: chi-squared =~ 0
    #95%: same
    #65%: chi-squared =~ 0.25
    CS = plt.contour(temps, intensities, S_values)
    plt.clabel(CS, inline=1, fontsize=10)
    plt.xlabel('Temperatures')
    plt.ylabel('Intensities')
    plt.title('Contour Plot of Chi-Squared Values')

    """
    fig3 = plt.figure()
    ax5 = fig3.contour(temps, intensities, S_values)
    ax5.set_ylabel('Intensities')
    ax5.set_xlabel('Temperatures')
    ax5.set_title('Contour Plot of Chi-Squared Values')
    """
    plt.savefig("hw10_contour.jpg")
    plt.show()
    
    print "---------------------------------------------"     
        
if __name__ == "__main__":
    main()