"""
Write a main program that:
1. opens and reads the data into x,y pairs
2. Estimates the best linear fit values for A,B and r as explained in Taylor.
PDF page 201
3. Plot the points with an overplot of the best fit line.

Look up the confidence with which you can conclude that there is a correlation. 
Then use the computer to get the same number.

Turn in your work to Baylee as a jpg plot, with A, B, r, and confidence 
printed on the plot.
"""

import numpy as np
import scipy as sp
from scipy.stats.stats import pearsonr
import os
import matplotlib.pyplot as plt

#read in and save data
os.chdir('C:/Users/Sierra/Documents/A_SCHOOL_7/sem10-spring2015/ASTR3800-AstrDataAnalysis/hw7/')
data = np.genfromtxt('correl.txt',dtype=float).T
x = data[0]
y = data[1]
N = len(x)
print "first x,y: ",x[0],y[0]
print "length of data: ",N

#Least-squares linear fit as defined on page 204 of the PDF:
delta = (N*sum(x**2))-((sum(x))**2)
print "delta: ",delta
A = ((sum(x**2)*sum(y))-(sum(x)*sum(x*y)))/delta
print "A: ",A
B = ((N*sum(x*y))-(sum(x)*sum(y)))/delta
print "B: ",B
x_bf = np.linspace(min(x),max(x),1000)
y_bf = A+(x_bf*B)

#correlation coefficient = covariance / product of std devs
#PDF p. 237
#r = np.cov(x,y)/(np.std(x)*np.std(y)) returns a matrix?
cov = 0
xm = np.mean(x)
ym = np.mean(y)
for i in xrange(0, N):
    cov = cov + ((x[i]-xm)*(y[i]-ym))
cov = cov/N
print "covariance: ",cov

r = cov/(np.std(x)*np.std(y))
print "correlation coefficient: ",r #.931, highly correlated

print "confidence from Appendix C for this r: >99.5 %" #p 311 of PDF

#calculate in Python: ***
conf = pearsonr(x,y) #np.corrcoef(x,y)[0,1] #pearsonr(x,y)
#gives a correlation coefficient and p-value for probability that they're not 
#correlated but give that r. p-values are not entirely
#reliable but are probably reasonable for datasets larger than 500 or so.
print "correlation coefficient using built-in function: ",conf[0]
print "probability that they're not correlated: ",conf[1]*100," %"
print "confidence (1-p) using built-in function: ",1.0-conf[1]

plt.plot(x, y, '.')
plt.plot(x_bf,y_bf)
plt.ylabel("Y")
plt.xlabel("X")
plt.title("Sierra Flynn's Correlation Plot")
txt = 'A: '+str(A)+'\nB: '+str(B)+'\nr: '+str(r)+'\nconfidence: ~'+str(1-conf[1])
plt.text(7, 16, txt)
plt.savefig("hw7plot.jpg")
plt.show() #*** print and turn in

