Homework 10 written answers
Sierra Flynn

First of all, this is probably not a final submission as I don't really
understand this homework and what you're looking for and how we're supposed
to go about doing it.

First, fit a blackbody spectrum. Find S_min. What is the best fit temperature? 
Is this a good model fit?

Based on the data and Wein's law alone, the best fit temperature is about
5.57*10^6 K (~5.5 million?!). Using a loop that goes through temperatures near
this value, and using Plank's equation for the black body spectrum, I got that
Tbest = 2.35*10^6 K and Smin = 9.4&10^12; no matter my range of T values,
S was always huge; this spectrum does not fit the data well.

Then try a free-free bremsstrahlung spectrum (j d_nu = exp(-h*nu/k/T) d_nu). 
What is the best fit temperature? Is this a significantly better fit than a 
blackbody? Is this a good model fit?

Using the ame approach, the best T was about 5.59*10^6, very close to the 
original data's probable temperature. The min S value for this was 0.171.
A S-value close to 0 indicates a very good fit!

With the better fit model, estimate the range of temperatures that will 
include the "true" temperature, 95% of the time. To do this, create a 100x100 
array of S values around S_min in the two free parameters of intensity and 
temperature. Make a contour plot of the two-dimensional confidence interval. 
The countours should be at 65%, 95% and 99.5% confidence levels. Be certain 
to label axes and make it look professional!

Using the free-free bremsstrahlung spectrum as the better fit, and looking at 
the contour plot, one can see that S-values ~= 0 are 90-100% confident while 
those around 0.25 are ~62% confident. (See table 12.6).
