"""
Hw3:
Create a main program in which you call a blackbody plotting subroutine. 
In the main, you give the surface temperature of a star, its radius in m, 
and its distance in pc. Create a plot of the flux at the Earth from that 
star by a one-line call to that procedure. Overplot two other stars of 
different temperature and distance with subsequent calls to the same 
procedure from the main.

The spectral plotting procedure must create a plot of the flux. The plot must 
have all axes labelled and a title. It must be in appropriate units. 
The procedure must plot the flux in a color that is appropriate to the star
(e.g. the Sun should be yellow).

The idea here is to make the plot as cool-looking as possible and exercise 
all the capabilities of Python's plot function.
"""
"""
hw 4:
Continue with Blackbody Plot
Do plot of Counts per bin for the case of the Sun at 10pc.
Do a check to recover the total luminosity of the Sun.
"""
import numpy as np
import matplotlib.pyplot as plt
#import os
from scipy.integrate import simps
#import pylab as pl
#import time
#pl.ion()

#constants:
kb = np.float64(1.38*(10**-23)) #boltzmann constant in J/K
h = np.float64(6.626*(10**-34))#plank's constant in J*s
c = np.float64(3*(10**8))#speed of light in m/s
sbc = np.float64(5.67*(10**-8)) #stefan-boltzman constant

#stefan-boltzman law, to check:
def stef_boltz_law(st, r, d):
    ratio = (r**2)/(d**2)
    return ratio*sbc*(st**4)

#define spectral radiance / plank's function:
def spec_rad(w, st):
    exp = np.float64(np.exp(h*c/(w*kb*st)))
    num = np.float64(2*h*(c**2))
    val = np.float64(num/((w**5)*(exp-1)))
    return val
    #W*sr^-1*m^-3.
     
#define flux for wavelength w:   
def flux(w, r, d, st):
    #spectral radiance in W/sr/m^2/m. We want flux in W/m^2.
    sa_atr = np.float64(4*np.pi*(r**2)) #m^2, to cancel out the term in spec_rad
    sa_atd = np.float64(4*np.pi*(d**2)) #m^2, to get the flux at d
    sr = np.float64(1*np.pi) #sr, to cancel out the steradian***
    #***1 because true SA of star = 4pi*r^2, but at our distance
    #we only see it as a circle with SA = pi*r^2?
    rad_flux = np.float64(spec_rad(w, st)*sr*(sa_atr/sa_atd)) #W/m^3
    val = rad_flux
    return val #W/m^3 for a wavelength
    #integral of W/m^3 should give 1400 for earth.

#plot blackbody function:
def plot_blackbody_flux(name, st, r, d):
    
    #Determine the color of the star:
    if st > 11000: color = 'blue' #blue
    if st < 11000 and st > 7500: color = 'gray' #white
    if st < 7500 and st > 5000: color = 'yellow' #yellow
    if st < 5000 and st > 3600: color = 'orange' #orange
    if st < 3600: color = 'red' #red
    
    #define wavelength and flux arrays:    
    w0 = 50. #angstroms
    w1 = 50000. #angstroms
    size = 1000 #want 1,000 data points
    incr = (w1-w0)/size
    
    flux_arr = np.arange(size, dtype=np.float64)
    w_arr = np.arange(size, dtype=np.float64)
    w_arr[0] = w0
    #fill wavelength array
    for i in range(1, size):
        w_arr[i] = w_arr[i-1]+incr
    w_arr = np.float64(w_arr*(10**-10)) #get to meters
 
    #calculate spectral flux at each wavelength   
    for i in range(0, size):
        flux_arr[i]=flux(w_arr[i],r,d,st)
        
    #find maximum flux at peak wavelength
    maxflux = np.max(flux_arr)
    k = np.where(flux_arr == maxflux)
    peakw = w_arr[k]
    maxflux = maxflux*peakw
    
    #integrate to get total flux:
    tot_flux = simps(flux_arr, w_arr)
    lum = tot_flux*(4*np.pi*d**2)
    sol_lum = 3.846*10**26 #watts
    sbf = stef_boltz_law(st, r, d)
    
    print "-------------------------------------"
    print "Flux Profile for "+name+" :"
    print "Maximum flux at peak wavelength: ",maxflux," W/m^2"
    print "peak wavelength: ",peakw," m"
    print "Max wavelength from Wein's Law: ",0.0029/st," m"
    print "Error: ",(np.abs(peakw-(0.0029/st))/(0.0029/st))*100,"%"
    print "TOTAL FLUX (integrated): ",tot_flux,"W/m^2"
    print "Flux from SB Law: ",sbf,"W/m^2"
    print "Error: ",(np.abs(sbf-tot_flux)/sbf)*100,"%"
    print "Luminosity: ",lum
    print "Solar Luminosities: ",lum/sol_lum
    print "Error: ",(np.abs(lum-sol_lum)/sol_lum)*100,"%"
    
    #Plot flux (W/m^2) vs. wavelength:
    #http://matplotlib.org/examples/style_sheets/plot_ggplot.html
    #fig, axes = plt.subplots(ncols=2, nrows=2)
    #ax1, ax2, ax3, ax4 = axes.ravel()***
    fig, axes = plt.subplots(ncols=2, nrows=1)
    ax1, ax2 = axes.ravel()
    
    ax1.plot(w_arr,flux_arr*w_arr,c=color,label=name)
    ax1.set_ylabel("Flux (W/m^2)")
    ax1.set_xlabel("Wavelength (m)")
    ax1.set_title("Flux at Planet @ 10pc (W/m^2)")
    
    #plot in wavelength space***
    ph_E = h*c/w_arr #photon energy in J, or (J*s)*(m/s)/m = J
    counts2 = (flux_arr*w_arr)/ph_E 
    #(W/m^3)*m/J = (W/m^2)*(s/W) = s/m^2 = counts per m^2 per sec
    n = 50 #some integer, for wavelengths per bin
    J = len(w_arr)/n #number of bins
    #print "num of bins: ",J
    dw = w_arr[1]-w_arr[0] #dist between wavelengths (m)
    #print "dist between wavelengths: ",dw
    Dw = n*dw #bin width in pixels?
    #print "bin width: ",Dw
    w_cent = np.zeros(J)
    flux_counts = np.zeros(J)
    for j in xrange(0,J): w_cent[j] = w_arr[j*n+2]#w_arr[0]+Dw*((w_arr[j-1]+w_arr[j])/2)
    #print "first few wavelengths: ",w_arr[0],w_arr[1],w_arr[2]
    #print "central wavelengths: ",w_cent[0], w_cent[1]
    for j in xrange(0,J): flux_counts[j] = counts2[(j*n)]+counts2[(j*n)+1]+counts2[(j*n)+2]+counts2[(j*n)+3]+counts2[(j*n)+4]
    ax2.bar(w_cent-Dw/2, flux_counts, Dw, color='r') #subtract Dw/2 to move it left
    
    #flux_arr2 = np.arange(size, dtype=np.float64)
    #for i in xrange(0, size): flux_arr2[i] = flux_arr[i]*w_arr[i]
    #ax2.hist(flux_arr2,bins=20)
    ax2.set_xlabel("Central Wavelength (m)")
    ax2.set_ylabel("Counts/m^2/s")
    ax2.set_title("Count per Bin")
    ax2.set_xlim([0,5*10**-6])
    #ax2.margins(0)
    #plt.savefig("BlackHole_LuminosityVSMass.png")
    #plt.legend()
    #plt.grid(True)
    #plt.ylim([0,0.0000000005])
    #plt.axhline(y=1395, color='red',linestyle='--') #Earth Flux
    #plt.axvline(x=6.35*(10**-7), color='red',linestyle='--') #Sun's peak wl
    
    """
    #Plot flux (W/m^3) vs. wavelength:
    ax3.plot(w_arr,flux_arr,c=color,label=name)
    ax3.set_ylabel("Flux (W/m^3)")
    ax3.set_xlabel("Wavelength (m)")
    ax3.set_title("Flux at Planet @ 10pc (W/m^3)")

    #plot in wavelength space***
    counts4 = counts2/w_arr #[(W/m^3)*m/J]/m = counts per m^3 per sec
    flux_counts4 = np.zeros(J)
    for j in xrange(0,J): flux_counts4[j] = counts4[(j*n)]+counts4[(j*n)+1]+counts4[(j*n)+2]+counts4[(j*n)+3]+counts4[(j*n)+4]
    ax4.bar(w_cent, flux_counts4, Dw, color='r')

    #ax4.hist(flux_arr,bins=20)
    ax4.set_xlabel("Central Wavelength (m)")
    ax4.set_ylabel("Counts/m^3/s")
    ax4.set_title("Counts per Bin")
"""
    #fix formatting
    plt.subplots_adjust(hspace=0.7)
    ax1.xaxis.major.formatter.set_powerlimits((0,0)) 
    ax1.yaxis.major.formatter.set_powerlimits((0,0)) 
    ax2.xaxis.major.formatter.set_powerlimits((0,0)) 
    ax2.yaxis.major.formatter.set_powerlimits((0,0)) 
    """
    ax3.xaxis.major.formatter.set_powerlimits((0,0)) 
    ax3.yaxis.major.formatter.set_powerlimits((0,0)) 
    ax4.xaxis.major.formatter.set_powerlimits((0,0)) 
    ax4.yaxis.major.formatter.set_powerlimits((0,0)) 
    """
    plt.show() 

def main():
    #for the sun at some planet:
    name = "Planet at 10 pc"
    st = 5800. #surface temp of sun in K
    r = 6.995*(10**8) #radius of sun in m
    #note 1 au = 5*10**-6 pc.
    au = 1.496*(10**11) #m
    d = np.float64(10.*(au/(4.848*10**-6))) #distance to sun in m, = 1 AU
    plot_blackbody_flux(name, st, r, d)    
    #NOTE TO GRADER: To see plots from homework 3, 
    #for some other stars, see hw3_Python.py.                        
      
if __name__ == "__main__":
    main()
    