# -*- coding: utf-8 -*-
"""
ASTR 3800 - HW 2 - Programming Assignment
Sierra Flynn
Due Tues 2/3/15

Create the following graph using Python. 
Run your program on both your laptop and Cosmos if possible

define x=[1.,2.,3.,4.], and 
y=[1.,7.,3.2,9.7], and 
dy=[0.1,0.2,0.15,0.3]

Plot y against x using a diamond symbol. 
At each point in x plot a line from y+dy to y-dy to show dy as the error bar.
"""

import matplotlib.pyplot as plt
import os

os.chdir('C:/Users/Sierra/Documents/A_SCHOOL_7/sem10-spring2015/ASTR3800-AstrDataAnalysis/')

x=[1.,2.,3.,4.]
y=[1.,7.,3.2,9.7]
dy=[0.1,0.2,0.15,0.3]

plt.plot(x,y,'rD')
plt.errorbar(x,y,yerr=dy,color='blue') #ERROR BARS!
plt.show()
plt.savefig("hw2_python.png")