"""
Create a main program in which you call a blackbody plotting subroutine. 
In the main, you give the surface temperature of a star, its radius in m, 
and its distance in pc. Create a plot of the flux at the Earth from that 
star by a one-line call to that procedure. Overplot two other stars of 
different temperature and distance with subsequent calls to the same 
procedure from the main.

The spectral plotting procedure must create a plot of the flux. The plot must 
have all axes labelled and a title. It must be in appropriate units. 
The procedure must plot the flux in a color that is appropriate to the star
(e.g. the Sun should be yellow).

The idea here is to make the plot as cool-looking as possible and exercise 
all the capabilities of Python's plot function.
"""
import numpy as np
import matplotlib.pyplot as plt
#from astropy.io import fits as pf #if 
import os
#import fitter
from scipy.integrate import simps

#constants:
kb = np.float64(1.38*(10**-23)) #boltzmann constant in J/K
h = np.float64(6.626*(10**-34))#plank's constant
c = np.float64(3*(10**8))#speed of light
sbc = np.float64(5.67*(10**-8)) #stefan-boltzman constant

def stef_boltz_law(st, r, d):
    ratio = (r**2)/(d**2)
    return ratio*sbc*(st**4)

#define spectral radiance / plank's function:
def spec_rad(w, st):
    exp = np.float64(np.exp(h*c/(w*kb*st)))
    num = np.float64(2*h*(c**2))
    val = np.float64(num/((w**5)*(exp-1)))
    return val
    #W*sr^-1*m^-3. This function is accurate for Earth.
     
#define flux for wavelength w:   
def flux(w, r, d, st):
    #spectral radiance in W/sr/m^2/m. We want flux in W/m^2.
    sa_atr = np.float64(4*np.pi*(r**2)) #m^2, to cancel out the term in spec_rad
    sa_atd = np.float64(4*np.pi*(d**2)) #m^2, to get the flux at d
    sr = np.float64(1*np.pi) #sr, to cancel out the steradian ***
    rad_flux = np.float64(spec_rad(w, st)*sr*(sa_atr/sa_atd)) #W/m^3
    val = rad_flux#*w
    return val #W/m^3 for a wavelength
    #should we plot w/m^3 or w/m^2?***
    #integral of W/m^3 should give 1400 for earth.

#plot blackbody function:
def plot_blackbody_flux(name, st, r, d):
    
    #Determine the color of the star:
    if st > 11000: color = 'blue' #blue
    if st < 11000 and st > 7500: color = 'gray' #white
    if st < 7500 and st > 5000: color = 'yellow' #yellow
    if st < 5000 and st > 3600: color = 'orange' #orange
    if st < 3600: color = 'red' #red
    
    rsun = 6.995*(10**8)
    scale = r/(rsun/10)
    
    #define wavelength and flux arrays:
    w0 = 100 #angstroms
    w1 = 30000 #angstroms
    size = 1000 #want 1,000 data points
    incr = (w1-w0)/size
    
    flux_arr = np.arange(size, dtype=np.float64)
    w_arr = np.arange(size, dtype=np.float64)
    w_arr[0] = w0
    for i in range(1, size):
        w_arr[i] = w_arr[i-1]+incr
    w_arr = np.float64(w_arr*(10**-10)) #get to meters
    
    for i in range(0, size-1):
        flux_arr[i]=flux(w_arr[i],r,d,st)
    maxflux = np.max(flux_arr)
    k = np.where(flux_arr == maxflux)
    peakw = w_arr[k]
    maxflux = maxflux*peakw #***
    print "-------------------------------------"
    print "Flux Profile for "+name
    print "maximum flux at peak wavelength: ",maxflux," W/m^2"
    print "peak wavelength: ",peakw," m"
    #integrate to get total flux:
    tot_flux = simps(flux_arr, w_arr)
    print "TOTAL FLUX: ",tot_flux,"W/m^2"
    sbf = stef_boltz_law(st, r, d)
    print "Flux from SB Law: ",sbf,"W/m^2"
    print "Deviance: ",sbf-tot_flux
    
    #Plot flux (W/m^2) vs. wavelength:
    plt.subplot(211)
    plt.plot(w_arr,flux_arr*w_arr,c=color,label=name)
    plt.ylabel("Flux (W/m^2)")
    plt.xlabel("Wavelength (m)")
    plt.title("Flux at Different Planets (W/m^2)")
    #plt.savefig("BlackHole_LuminosityVSMass.png")
    plt.legend()
    plt.grid(True)
    plt.axhline(y=1395, color='red',linestyle='--') #Earth Flux
    plt.axvline(x=6.35*(10**-7), color='red',linestyle='--') #Sun's peak wl
    plt.show()
    
    #Plot flux (W/m^3) vs. wavelength:
    plt.subplot(212)
    plt.plot(w_arr,flux_arr,c=color,label=name)
    plt.ylabel("Flux (W/m^3)")
    plt.xlabel("Wavelength (m)")
    plt.title("Flux at Different Planets (W/m^3)")
    #plt.savefig("BlackHole_LuminosityVSMass.png")
    plt.legend()
    plt.grid(True)
    plt.axhline(y=1395, color='red',linestyle='--') #Earth Flux
    plt.axvline(x=6.35*(10**-7), color='red',linestyle='--') #Sun's peak wl
    plt.show() 

def main():
    #for the sun at Earth:
    namee = "Earth"
    ste = 5800. #surface temp of sun in K
    re = 6.995*(10**8) #radius of sun in m
    de = 1.5*(10**11) #distance to sun in m, = 1 AU
    plot_blackbody_flux(namee, ste, re, de)
    
    #for Kepler-62f, a habitable planet orbiting kepler-62:
    namef = "Kepler-62f"
    stf = 4925. #surface temp of star in K
    rf = .64*re #radius of star in m
    df = .718*de #distance to star in m
    plot_blackbody_flux(namef, stf, rf, df)
    
    #other random stars:
    namex = "Planet X"
    stx = 20000 #surface temp of star in K
    rx = 6*re #radius of star in m
    dx = 100*de #distance to star in m
    plot_blackbody_flux(namex, stx, rx, dx)

    namey = "Planet Y"
    sty = 3000 #surface temp of star in K
    ry = .1*re #radius of star in m
    dy = .04*de #distance to star in m
    plot_blackbody_flux(namey, sty, ry, dy)
    
    namez = "Planet Z"
    stz = 10000 #surface temp of star in K
    rz = 5*re #radius of star in m
    dz = 15*de #distance to star in m
    plot_blackbody_flux(namez, stz, rz, dz)
                            
      
if __name__ == "__main__":
    main()
    